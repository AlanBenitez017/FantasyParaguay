//
//  ViewControllerPickTeam.swift
//  FantasyParaguay
//
//  Created by Alan Benitez on 2023-04-12.
//

import Foundation
import UIKit

class ViewControllerPickTeam: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    var flag = 0;
    
    // TITULARES
    @IBOutlet weak var goalkeeperTitular: UIStackView!
    
    @IBOutlet weak var defense1Titular: UIStackView!
    @IBOutlet weak var defense2Titular: UIStackView!
    @IBOutlet weak var defense3Titular: UIStackView!
    @IBOutlet weak var defense4Titular: UIStackView!
    
    @IBOutlet weak var midfielder1Titular: UIStackView!
    @IBOutlet weak var midfielder2Titular: UIStackView!
    @IBOutlet weak var midfielder3Titular: UIStackView!
    
    @IBOutlet weak var striker1Titular: UIStackView!
    @IBOutlet weak var striker2Titular: UIStackView!
    @IBOutlet weak var striker3Titular: UIStackView!
    
    // SUPLENTES
    @IBOutlet weak var goalkeeperSuplente: UIStackView!
    @IBOutlet weak var defense5Suplente: UIStackView!
    @IBOutlet weak var midfielder4Suplente: UIStackView!
    @IBOutlet weak var midfielder5Suplente: UIStackView!
    
    var temp = UIStackView()

    // STACKVIEWS
    @IBOutlet weak var stackViewArqueroTitular: UIStackView!
    @IBOutlet weak var stackViewDefensasTitulares: UIStackView!
    @IBOutlet weak var stackViewMidfieldersTitulares: UIStackView!
    @IBOutlet weak var stackViewStrikersTitulares: UIStackView!
    @IBOutlet weak var stackViewSuplentes: UIStackView!
    
    var posicion = ""
    var primerJugadorPresionado = ""
    var jugadorTemp = UIStackView()
    var defenseCounter = 4
    var midfielderCounter = 3
    var strikerCounter = 3
    
    @IBAction func goalkeeperTitular(_ sender: Any) {
        print("gk titular")
        flag += 1
        print("supuesto flag = 1", flag)
        posicion = "arq"
        if(flag == 1){
            setPosicion(posicion)
            setJugador(goalkeeperTitular)
        }
        print(flag)
        //var primerJugadorPresionado = getJugador()
        if(flag == 2){
            //temp = goalkeeperTitular
            var primerJugadorPresionado = getJugador()
            switchPlayers(&primerJugadorPresionado, &goalkeeperTitular, posicion)
            /*stackViewSuplentes.removeArrangedSubview(goalkeeperTitular)
            stackViewSuplentes.addArrangedSubview(goalkeeperSuplente)
            stackViewArqueroTitular.addArrangedSubview(goalkeeperTitular)*/
   
            //switchPlayers(&primerJugadorPresionado, &goalkeeperTitular, posicion)
            /*temp = goalkeeperSuplente
            goalkeeperSuplente = goalkeeperTitular
            goalkeeperTitular = temp*/
            /*goalkeeperTitular.addSubview(goalkeeperSuplente)// goalkeeperSuplente
            goalkeeperSuplente = temp*/
            
            //stackViewSuplentes.addSubview(goalkeeperTitular)
            //stackViewArqueroTitular.addSubview(goalkeeperSuplente)
            
            /*
              Funciona maso
            temp = goalkeeperTitular
            goalkeeperTitular.addSubview(goalkeeperSuplente)// goalkeeperSuplente
            goalkeeperSuplente = temp */
            //goalkeeperSuplente = temp
            flag = 0
        }
    }
    
    @IBAction func defense1Titular(_ sender: Any) {
        print("def1 titular")
        posicion = "def"
        flag += 1
        if(flag == 1){
            setPosicion(posicion)
            setJugador(defense1Titular)
        }
        print(flag)
        if(flag == 2){
            //temp = goalkeeperTitular
            var primerJugadorPresionado = getJugador()
            switchPlayers(&primerJugadorPresionado, &defense1Titular, posicion)
//            stackViewSuplentes.removeArrangedSubview(goalkeeperSuplente)
//            stackViewSuplentes.addArrangedSubview(goalkeeperTitular)
//            stackViewArqueroTitular.addArrangedSubview(goalkeeperSuplente)
//            temp = goalkeeperTitular
//            goalkeeperTitular = goalkeeperSuplente
//            goalkeeperSuplente = temp
            /*goalkeeperTitular.addSubview(goalkeeperSuplente)// goalkeeperSuplente
            goalkeeperSuplente = temp*/
            
            //stackViewSuplentes.addSubview(goalkeeperTitular)
            //stackViewArqueroTitular.addSubview(goalkeeperSuplente)
            
            /*
              Funciona maso
            temp = goalkeeperTitular
            goalkeeperTitular.addSubview(goalkeeperSuplente)// goalkeeperSuplente
            goalkeeperSuplente = temp */
            //goalkeeperSuplente = temp
            flag = 0
        }

    }
    
    @IBAction func defense2Titular(_ sender: Any) {
        posicion = "def"
        flag+=1
        if(flag == 1){
            setPosicion(posicion)
            setJugador(defense2Titular)
        }
        print(flag)
        if(flag == 2){
            var primerJugadorPresionado = getJugador()
            switchPlayers(&primerJugadorPresionado, &defense2Titular, posicion)
            flag = 0
        }


    }
    
    @IBAction func defense3Titular(_ sender: Any) {
        print("def3 titular")
        posicion = "def"
        flag+=1
        if(flag == 1){
            setPosicion(posicion)
            setJugador(defense3Titular)
        }
        print(flag)
        if(flag == 2){
            var primerJugadorPresionado = getJugador()
            switchPlayers(&primerJugadorPresionado, &defense3Titular, posicion)
            flag = 0
        
        }

    }
    
    @IBAction func defense4Titular(_ sender: Any) {
        print("def4 titular")
        posicion = "def"
        flag+=1
        if(flag == 1){
            setPosicion(posicion)
            setJugador(defense4Titular)
        }
        print(flag)
        if(flag == 2){
            var primerJugadorPresionado = getJugador()
            switchPlayers(&primerJugadorPresionado, &defense4Titular, posicion)
            flag = 0
        
        }
    }
    
    @IBAction func midfielder1Titular(_ sender: Any) {
        print("mid1 titular")
        posicion = "mid"
        flag+=1
        if(flag == 1){
            setPosicion(posicion)
            setJugador(midfielder1Titular)
        }
        print(flag)
        if(flag == 2){
            var primerJugadorPresionado = getJugador()
            switchPlayers(&primerJugadorPresionado, &midfielder1Titular, posicion)
            flag = 0
        
        }

    }
    
    @IBAction func midfielder2Titular(_ sender: Any) {
        print("mid2 titular")
        posicion = "mid"
        flag+=1
        if(flag == 1){
            setPosicion(posicion)
            setJugador(midfielder2Titular)
        }
        print(flag)
        if(flag == 2){
            var primerJugadorPresionado = getJugador()
            switchPlayers(&primerJugadorPresionado, &midfielder2Titular, posicion)
            flag = 0
        
        }

    }
    
    @IBAction func midfielder3Titular(_ sender: Any) {
        print("mid3 titular")
        posicion = "mid"
        flag+=1
        if(flag == 1){
            setPosicion(posicion)
            setJugador(midfielder3Titular)
        }
        print(flag)
        if(flag == 2){
            var primerJugadorPresionado = getJugador()
            switchPlayers(&primerJugadorPresionado, &midfielder3Titular, posicion)
            flag = 0
        
        }

    }
    
    @IBAction func striker1Titular(_ sender: Any) {
        print("str1 titular")
        posicion = "str"
        flag+=1
        if(flag == 1){
            setPosicion(posicion)
            setJugador(striker1Titular)
        }
        print(flag)
        if(flag == 2){
            var primerJugadorPresionado = getJugador()
            switchPlayers(&primerJugadorPresionado, &striker1Titular, posicion)
            flag = 0
        
        }

    }
    
    @IBAction func striker2Titular(_ sender: Any) {
        print("str2 titular")
        posicion = "str"
        flag+=1
        if(flag == 1){
            setPosicion(posicion)
            setJugador(striker2Titular)
        }
        print(flag)
        if(flag == 2){
            var primerJugadorPresionado = getJugador()
            switchPlayers(&primerJugadorPresionado, &striker2Titular, posicion)
            flag = 0
        
        }

    }
    
    @IBAction func striker3Titular(_ sender: Any) {
        print("str3 titular")
        posicion = "str"
        flag+=1
        if(flag == 1){
            setPosicion(posicion)
            setJugador(striker3Titular)
        }
        print(flag)
        if(flag == 2){
            var primerJugadorPresionado = getJugador()
            switchPlayers(&primerJugadorPresionado, &striker3Titular, posicion)
            flag = 0
        
        }
    }
    
    @IBAction func goalkeeper2Sup(_ sender: Any) {
        print("gk2 suplente")
        posicion = "arq"
        flag += 1
        print("supuesto flag = 1", flag)
        if(flag == 1){
            setPosicion(posicion)
            setJugador(goalkeeperSuplente)
        }
        if(flag == 2){
            //temp = goalkeeperTitular
            //goalkeeperTitular.removeFromSuperview()
            var primerJugadorPresionado = getJugador()
            switchPlayers(&primerJugadorPresionado, &goalkeeperSuplente, posicion)
        
             
            /*stackViewArqueroTitular.removeArrangedSubview(goalkeeperTitular)
            stackViewArqueroTitular.addArrangedSubview(goalkeeperSuplente)
            stackViewSuplentes.addArrangedSubview(goalkeeperTitular)*/
            //stackViewSuplentes.addSubview(goalkeeperTitular)
            
            
            
            //goalkeeperSuplente = temp
            //var primerJugadorPresionado = getJugador()

            //switchPlayers(&primerJugadorPresionado, &goalkeeperSuplente, posicion)
            /*temp = goalkeeperTitular
            goalkeeperTitular = goalkeeperSuplente
            goalkeeperSuplente = temp*/
            // goalkeeperSuplente
            //goalkeeperTitular//.removeArrangedSubview(goalkeeperTitular)
            //goalkeeperTitular.removeFromSuperview()

            //goalkeeperSuplente.addSubview(temp)
            //goalkeeperSuplente = temp

            /*
             Funciona maso
            temp = goalkeeperTitular
            goalkeeperTitular.addSubview(goalkeeperSuplente)// goalkeeperSuplente
            goalkeeperSuplente = temp
             */
            /*goalkeeperSuplente.addArrangedSubview(goalkeeperTitular)
            goalkeeperSuplente.removeFromSuperview()
            
            goalkeeperTitular.addArrangedSubview(goalkeeperSuplente)
            goalkeeperTitular.removeFromSuperview()*/
            //goalkeeperSuplente = temp
            flag = 0
        }
    }
    
    @IBAction func defense5Sup(_ sender: UIStackView) {
        print("def5 suplente")
        posicion = "def"
        flag+=1
        if(flag == 1){
            setPosicion(posicion)
            setJugador(defense5Suplente)
        }
        print(flag)
        if(flag == 2){
            var primerJugadorPresionado = getJugador()
            switchPlayers(&primerJugadorPresionado, &defense5Suplente, posicion)
            flag = 0
        
        }

    }
    
    @IBAction func midfielder4Sup(_ sender: Any) {
        print("mid4 suplente")
        posicion = "mid"
        flag+=1
        if(flag == 1){
            setPosicion(posicion)
            setJugador(midfielder4Suplente)
        }
        print(flag)
        if(flag == 2){
            var primerJugadorPresionado = getJugador()
            switchPlayers(&primerJugadorPresionado, &midfielder4Suplente, posicion)
            flag = 0
        
        }

    }
    
    @IBAction func midfielder5Sup(_ sender: Any) {
        print("mid5 suplente")
        posicion = "mid"
        flag+=1
        if(flag == 1){
            setPosicion(posicion)
            setJugador(midfielder5Suplente)
        }
        print(flag)
        if(flag == 2){
            var primerJugadorPresionado = getJugador()
            switchPlayers(&primerJugadorPresionado, &midfielder5Suplente, posicion)
            flag = 0
        
        }


    }
    
    func switchPlayers(_ jugador: inout UIStackView, _ jugador2: inout UIStackView, _ posicion: String) {
        //var jugadorCopia = jugador
        //var jugadorCopia2 = jugador2
        print("Posicion primer jugador presionado:", getPosicion(), "Posicion segundo jugador presionado:", posicion)
        if posicion == "arq" && getPosicion() == "arq"{
            stackViewArqueroTitular.removeArrangedSubview(jugador)
            stackViewArqueroTitular.addArrangedSubview(jugador2)
            stackViewSuplentes.addArrangedSubview(jugador)
            /*temp = jugador
            jugador = jugador2
            jugador2 = temp*/
            
        } else if getPosicion() == "def" && (posicion == "mid" || posicion == "str" || posicion == "def") {
            stackViewDefensasTitulares.removeArrangedSubview(jugador)
            if posicion == "mid" {
                defenseCounter -= 1
                midfielderCounter += 1
                if defenseCounter <= 2 {
                    print("no pueden haber menos de 3 defensas")
                    defenseCounter += 1
                    midfielderCounter -= 1
                    stackViewDefensasTitulares.addArrangedSubview(jugador)
                }else{
                    print("Defense tit: ",defenseCounter)
                    print("Midfielder tit: ",midfielderCounter)
                    if defenseCounter == 5 {
                        /*
                            if midfielderCounter == 5{
                                // agg constraints
                            }
                            else if midfielderCounter == 4{
                                // agg constraints
                            }
                            else if midfielderCounter == 3 {
                                // agg constraints
                            }
                            else if midfielderCounter == 2 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    } else if defenseCounter == 4{
                        /*
                            if midfielderCounter == 5{
                                // agg constraints
                            }
                            else if midfielderCounter == 4{
                                // agg constraints
                            }
                            else if midfielderCounter == 3 {
                                // agg constraints
                            }
                            else if midfielderCounter == 2 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    } else if defenseCounter == 3 {
                        /*
                            if strikerCounter == 5{
                                // agg constraints
                            }
                            else if strikerCounter == 4{
                                // agg constraints
                            }
                            else if strikerCounter == 3 {
                                // agg constraints
                            }
                            else if midfielderCounter == 2 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    }
                    stackViewMidfieldersTitulares.addArrangedSubview(jugador2)
                    stackViewMidfieldersTitulares.distribution  = UIStackView.Distribution.fillEqually
                    stackViewSuplentes.addArrangedSubview(jugador)

                }
            } else if posicion == "str" {
                defenseCounter -= 1
                strikerCounter += 1
                if defenseCounter <= 2 {
                    print("no pueden haber menos de 3 defensas")
                    defenseCounter += 1
                    strikerCounter -= 1
                    stackViewDefensasTitulares.addArrangedSubview(jugador)
                }else{
                    print("Defense tit: ",defenseCounter)
                    print("Striker tit: ",strikerCounter)
                    if defenseCounter == 5 {
                        /*
                            if strikerCounter == 3{
                                // agg constraints
                            }
                            else if strikerCounter == 2{
                                // agg constraints
                            }
                            else if strikerCounter == 1 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    } else if defenseCounter == 4{
                        /*
                            if strikerCounter == 3{
                                // agg constraints
                            }
                            else if strikerCounter == 2{
                                // agg constraints
                            }
                            else if strikerCounter == 1 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    } else if defenseCounter == 3 {
                        /*
                            if strikerCounter == 3{
                                // agg constraints
                            }
                            else if strikerCounter == 2{
                                // agg constraints
                            }
                            else if strikerCounter == 1 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    }
                    stackViewStrikersTitulares.addArrangedSubview(jugador2)
                    stackViewStrikersTitulares.distribution  = UIStackView.Distribution.fillEqually
                    stackViewSuplentes.addArrangedSubview(jugador)

                }
            } else {
                stackViewDefensasTitulares.addArrangedSubview(jugador2)
                stackViewSuplentes.addArrangedSubview(jugador)

            }
            //stackViewDefensasTitulares.removeArrangedSubview(jugador)
            //stackViewDefensasTitulares.addArrangedSubview(jugador2)
            //stackViewSuplentes.addArrangedSubview(jugador)     ESTE ESTABA BIEN
            /*temp = jugador
            jugador = jugador2
            jugador2 = temp*/
        } else if getPosicion() == "mid" && (posicion == "mid" || posicion == "str" || posicion == "def") {
            stackViewMidfieldersTitulares.removeArrangedSubview(jugador)

            if posicion == "str" {
                midfielderCounter -= 1
                strikerCounter += 1
                if midfielderCounter <= 1{
                    print("no pueden haber menos de 2 mediocampistas")
                    midfielderCounter += 1
                    strikerCounter -= 1
                    stackViewMidfieldersTitulares.addArrangedSubview(jugador)
                } else{
                    print("Midfielders tit: ",midfielderCounter)
                    print("Strikers tit: ",strikerCounter)
                    if midfielderCounter == 5 {
                        /*
                            if strikerCounter == 5{
                                // agg constraints
                            }
                            else if strikerCounter == 4{
                                // agg constraints
                            }
                            else if strikerCounter == 3 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    } else if midfielderCounter == 4 {
                        /*
                            if strikerCounter == 5{
                                // agg constraints
                            }
                            else if strikerCounter == 4{
                                // agg constraints
                            }
                            else if strikerCounter == 3 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    } else if midfielderCounter == 3 {
                        /*
                            if strikerCounter == 5{
                                // agg constraints
                            }
                            else if strikerCounter == 4{
                                // agg constraints
                            }
                            else if strikerCounter == 3 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    }
                    stackViewStrikersTitulares.addArrangedSubview(jugador2)
                    stackViewStrikersTitulares.distribution  = UIStackView.Distribution.fillEqually
                    stackViewSuplentes.addArrangedSubview(jugador)
                }
            } else if posicion == "def" {
                midfielderCounter -= 1
                defenseCounter += 1
                if midfielderCounter <= 1{
                    print("no pueden haber menos de 2 mediocampistas")
                    midfielderCounter += 1
                    defenseCounter -= 1
                    stackViewMidfieldersTitulares.addArrangedSubview(jugador)
                }else{
                    print("Midfielders tit: ",midfielderCounter)
                    print("Defense tit: ",defenseCounter)
                    if midfielderCounter == 5 {
                        /*
                            if defenseCounter == 5{
                                // agg constraints
                            }
                            else if defenseCounter == 4{
                                // agg constraints
                            }
                            else if defenseCounter == 3 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    } else if midfielderCounter == 4 {
                        /*
                            if defenseCounter == 5{
                                // agg constraints
                            }
                            else if defenseCounter == 4{
                                // agg constraints
                            }
                            else if defenseCounter == 3 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    } else if midfielderCounter == 3 {
                        /*
                            if defenseCounter == 5{
                                // agg constraints
                            }
                            else if defenseCounter == 4{
                                // agg constraints
                            }
                            else if defenseCounter == 3 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    } else {
                        /*
                            if defenseCounter == 5{
                                // agg constraints
                            }
                            else if defenseCounter == 4{
                                // agg constraints
                            }
                            else if defenseCounter == 3 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    }
                    stackViewDefensasTitulares.addArrangedSubview(jugador2)
                    stackViewDefensasTitulares.distribution  = UIStackView.Distribution.fillEqually
                    stackViewSuplentes.addArrangedSubview(jugador)

                }
                
            } else {
                stackViewMidfieldersTitulares.addArrangedSubview(jugador2)
                stackViewSuplentes.addArrangedSubview(jugador)

            }
            //stackViewSuplentes.addArrangedSubview(jugador)
            /*temp = jugador
            jugador = jugador2
            jugador2 = temp*/
        } else if getPosicion() == "str" && (posicion == "str" || posicion == "mid" || posicion == "def") {
            stackViewStrikersTitulares.removeArrangedSubview(jugador)

            if(posicion == "mid") {
                strikerCounter -= 1
                midfielderCounter += 1
                if strikerCounter < 1 {
                    print("no pueden haber menos de 1 delantero")
                    strikerCounter += 1
                    midfielderCounter -= 1
                    stackViewStrikersTitulares.addArrangedSubview(jugador)
                }else{
                    print("Strikers tit: ",strikerCounter)
                    print("Midfielders tit: ",midfielderCounter)
                    if strikerCounter == 3 {
                        /*
                            if midfielderCounter == 5{
                                // agg constraints
                            }
                            else if midfielderCounter == 4{
                                // agg constraints
                            }
                            else if midfielderCounter == 3 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    } else if strikerCounter == 2 {
                        /*
                            if midfielderCounter == 5{
                                // agg constraints
                            }
                            else if midfielderCounter == 4{
                                // agg constraints
                            }
                            else if midfielderCounter == 3 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    } else {
                        /*
                            if midfielderCounter == 5{
                                // agg constraints
                         //    view.addConstraint(NSLayoutConstraint(item: gamePreview, attribute: .Trailing, relatedBy: .Equal, toItem: view, attribute: .Trailing, multiplier: 1, constant: 0))

                            }
                            else if midfielderCounter == 4{
                                // agg constraints
                            }
                            else if midfielderCounter == 3 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    }
                    stackViewMidfieldersTitulares.addArrangedSubview(jugador2)
                    stackViewMidfieldersTitulares.distribution  = UIStackView.Distribution.fillEqually
                    stackViewSuplentes.addArrangedSubview(jugador)
                }
            } else if posicion == "def" {
                strikerCounter -= 1
                defenseCounter += 1
                if strikerCounter < 1 {
                    print("no pueden haber menos de 1 delantero")
                    strikerCounter += 1
                    defenseCounter -= 1
                    stackViewStrikersTitulares.addArrangedSubview(jugador)
                }else{
                    print("Striker tit: ",strikerCounter)
                    print("Defense tit: ",defenseCounter)
                    if strikerCounter == 3 {
                        /*
                            if defenseCounter == 5{
                                // agg constraints
                            }
                            else if defenseCounter == 4{
                                // agg constraints
                            }
                            else if defenseCounter == 3 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    } else if strikerCounter == 2 {
                        /*
                            if defenseCounter == 5{
                                // agg constraints
                            }
                            else if defenseCounter == 4{
                                // agg constraints
                            }
                            else if defenseCounter == 3 {
                                // agg constraints
                            }
                         etc etc etc
                         */
                    } else {
                        /*
                            if defenseCounter == 5{
                                // agg constraints
                            }
                            else if defenseCounter == 4{
                                // agg constraints
                            }
                            else if defenseCounter == 3 {
                                // agg constraints
                            }
                            etc etc etc
                         */
                    }
                    stackViewDefensasTitulares.addArrangedSubview(jugador2)
                    stackViewDefensasTitulares.distribution  = UIStackView.Distribution.fillEqually
                    stackViewSuplentes.addArrangedSubview(jugador)
                }
                
            } else {
                stackViewStrikersTitulares.addArrangedSubview(jugador2)
                stackViewSuplentes.addArrangedSubview(jugador)
            }
            //stackViewSuplentes.addArrangedSubview(jugador)
            /*temp = jugador
            jugador = jugador2
            jugador2 = temp*/
        } else{
            print("nada")
        }
        
    }
    
    
    
    func setPosicion(_ posicion: String){
        self.primerJugadorPresionado = posicion
    }
           
    func getPosicion() -> String{
        return primerJugadorPresionado
    }
    
    func setJugador(_ jugador: UIStackView){
        self.jugadorTemp = jugador
    }
    
    func getJugador() -> UIStackView{
        return jugadorTemp
    }
    
}
