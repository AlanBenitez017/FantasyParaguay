//
//  ViewControllerPlantilla.swift
//  FantasyParaguay
//
//  Created by Sebas on 2023-03-30.
//

import UIKit

class ViewControllerPlantilla: UIViewController {
    
    @IBOutlet weak var goalkeeper1Name: UILabel!
    @IBOutlet weak var goalkeeper1Cost: UILabel!
    @IBOutlet weak var goalkeeper1Pic: UIImageView!
    
    @IBOutlet weak var goalkeeper2Name: UILabel!
    @IBOutlet weak var goalkeeper2Cost: UILabel!
    @IBOutlet weak var goalkeeper2Pic: UIImageView!
    
    @IBOutlet weak var defender1Name: UILabel!
    @IBOutlet weak var defender1Cost: UILabel!
    @IBOutlet weak var defender1Pic: UIImageView!
    
    @IBOutlet weak var defender2Name: UILabel!
    @IBOutlet weak var defender2Cost: UILabel!
    @IBOutlet weak var defender2Pic: UIImageView!
    
    @IBOutlet weak var defender3Name: UILabel!
    @IBOutlet weak var defender3Cost: UILabel!
    @IBOutlet weak var defender3Pic: UIImageView!
    
    @IBOutlet weak var defender4Name: UILabel!
    @IBOutlet weak var defender4Cost: UILabel!
    @IBOutlet weak var defender4Pic: UIImageView!
    
    @IBOutlet weak var defender5Name: UILabel!
    @IBOutlet weak var defender5Cost: UILabel!
    @IBOutlet weak var defender5Pic: UIImageView!
    
    @IBOutlet weak var midfielder1Name: UILabel!
    @IBOutlet weak var midfielder1Cost: UILabel!
    @IBOutlet weak var midfielder1Pic: UIImageView!
    
    @IBOutlet weak var midfielder2Name: UILabel!
    @IBOutlet weak var midfielder2Cost: UILabel!
    @IBOutlet weak var midfielder2Pic: UIImageView!
    
    @IBOutlet weak var midfielder3Name: UILabel!
    @IBOutlet weak var midfielder3Cost: UILabel!
    @IBOutlet weak var midfielder3Pic: UIImageView!
    
    @IBOutlet weak var midfielder4Name: UILabel!
    @IBOutlet weak var midfielder4Cost: UILabel!
    @IBOutlet weak var midfielder4Pic: UIImageView!
    
    @IBOutlet weak var midfielder5Name: UILabel!
    @IBOutlet weak var midfielder5Cost: UILabel!
    @IBOutlet weak var midfielder5Pic: UIImageView!
    
    @IBOutlet weak var striker1Name: UILabel!
    @IBOutlet weak var striker1Cost: UILabel!
    @IBOutlet weak var striker1Pic: UIImageView!
    
    @IBOutlet weak var striker2Name: UILabel!
    @IBOutlet weak var striker2Cost: UILabel!
    @IBOutlet weak var striker2Pic: UIImageView!
    
    @IBOutlet weak var striker3Name: UILabel!
    @IBOutlet weak var striker3Cost: UILabel!
    @IBOutlet weak var striker3Pic: UIImageView!
    
    @IBOutlet weak var teamName: UILabel!
    @IBOutlet weak var dinero: UILabel!
    @IBOutlet weak var freeTransfers: UILabel!
    
    //@IBOutlet weak var fondo: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("hola2")
        //self.view.backgroundColor = UIColor(patternImage: UIImage(named: "Image.png")!)

        //let screenSize: CGRect = UIScreen.main.bounds
        //fondo.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        //popUpView.layer.cornerRadius = 24
        //popUpView.isHidden = true
    }
    
    @IBAction func goalkeeper1(_ sender: Any) {
        print("gk1 tapped")
    }
    
    @IBAction func goalkeeper2(_ sender: Any) {
        print("gk2 tapped")
    }
    
    @IBAction func defender1(_ sender: Any) {
        print("def1 tapped")
    }
    
    @IBAction func defender2(_ sender: Any) {
        print("def2 tapped")
    }
    
    @IBAction func defender3(_ sender: Any) {
        print("def3 tapped")
    }
    
    @IBAction func defender4(_ sender: Any) {
        print("def4 tapped")
    }
    
    @IBAction func defender5(_ sender: Any) {
        print("def5 tapped")
    }
    
    @IBAction func midfielder1(_ sender: Any) {
        print("mid1 tapped")
    }
    
    @IBAction func midfielder2(_ sender: Any) {
        print("mid2 tapped")
    }
    
    @IBAction func midfielder3(_ sender: Any) {
        print("mid3 tapped")
    }
    
    @IBAction func midfielder4(_ sender: Any) {
        print("mid4 tapped")
    }
    
    @IBAction func midfielder5(_ sender: Any) {
        print("mid5 tapped")
    }
    
    @IBAction func striker1(_ sender: Any) {
        print("stri1 tapped")
    }
    
    @IBAction func striker2(_ sender: Any) {
        print("stri2 tapped")
    }
    
    @IBAction func striker3(_ sender: Any) {
        print("stri3 tapped")
    }
    
    
    
    
}
